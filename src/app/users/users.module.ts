import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsersComponent } from './users.component';
import {RouterModule, Routes} from '@angular/router';
import { ActionComponent } from './action/action.component';
import { ViewComponent } from './view/view.component';
import {FormsModule} from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';


const userRoutes: Routes = [
  { path: 'users', component: UsersComponent},
  { path: 'users/create', component: ActionComponent},
  { path: 'users/:id', component: ViewComponent},
  { path: 'users/:id/edit', component: ActionComponent}
];

@NgModule({
  declarations: [UsersComponent, ActionComponent, ViewComponent],
  imports: [
    CommonModule,
    RouterModule.forRoot(userRoutes),
    FormsModule,
    NgSelectModule
  ]
})
export class UsersModule { }
