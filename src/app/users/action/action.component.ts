import {Component, OnInit} from '@angular/core';
import {BryService} from '../../bry.service';
import {ActivatedRoute, Router} from '@angular/router';
import {NotifyService} from '../../services/notify.service';
import {HttpErrorResponse} from '@angular/common/http';

@Component({
  selector: 'app-action',
  templateUrl: './action.component.html',
  styleUrls: ['./action.component.css']
})
export class ActionComponent implements OnInit {

  private idUser: number;
  private userForm = {};
  private organizationForm = [null];
  private organizations: any;
  private organizationsOfUser = [0];
  private loader: boolean = true;

  constructor(private service: BryService,
              private router: Router,
              private organizationsService: BryService,
              private route: ActivatedRoute,
              private notifyService: NotifyService) {

    this.organizationsService.createUrl('/organizations');
    this.organizationsService.groupList().subscribe((data) => {
      this.organizations = data;
    });


    this.route.params.subscribe((data) => this.idUser = data.id);


    this.service.createUrl('/users');
    if (this.idUser) {
      this.service.get(this.idUser).subscribe((data) => {
          this.userForm = data;
          if (this.userForm['organizations'].length > 0) {
            this.organizationForm = [];
            for (let org of this.userForm['organizations']) {
              this.organizationForm.push(org['id']);
            }
          }
          this.loader = false;
        }
      );
    } else {
      this.loader = false;
    }


  }

  ngOnInit() {
  }

  subSelectOrganization(index) {
    this.organizationsOfUser.splice(index, 1);
    this.organizationForm.splice(index, 1);
  }

  addSelectOrganization() {
    this.organizationForm.push(null);
  }

  saveForm() {

    //validate organizations
    this.userForm['organization_id'] = this.validateOrganizations();
    let requestService;
    let msg = '';
    if (this.idUser) {
      requestService = this.service.update(this.idUser, this.userForm);
      msg = 'Usuário alterado com sucesso';
    } else {
      requestService = this.service.create(this.userForm);
      msg = 'Usuário cadastrado com sucesso';
    }


    requestService.subscribe(() => {
      this.router.navigate(['/users']);
      this.notifyService.success(msg);
    }, (error) => {
      this.notifyService.listErrors(error);
    });

  }

  validateOrganizations() {
    let idO = [];
    for (let org of this.organizationForm) {
      if (Number.isInteger(org)) idO.push(org);
    }
    return idO;
  }

  deleteUser() {
    if (confirm('Realmente excluir o usuário?')) {
      this.loader = true;
      this.service.delete(this.idUser).subscribe(() => {
          this.router.navigate(['/users']);
        }
      );
    }
  }

}
