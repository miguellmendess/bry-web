import { Component, OnInit } from '@angular/core';
import {BryService} from '../bry.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  private users: any;
  private loader: boolean = true;
  constructor(private service: BryService, private router: Router) {
    this.service.createUrl('/users');
  }

  ngOnInit() {
    this.service.groupList().subscribe((data)=>{
      this.users = data;
      this.loader = false;
    });
  }

  createUser(){
    this.router.navigate(['/users/create']);
  }

  viewUser(id){
    this.router.navigate(['/users/'+id]);
  }

  deleteUser(id){
    if(confirm("Realmente excluir o usuário?")){
      this.loader = true;
      this.service.delete(id).subscribe((data) => {
        this.users = data;
        this.loader = false;
        }
      );
    }
  }

}
