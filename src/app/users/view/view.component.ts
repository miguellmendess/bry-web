import {Component, OnInit} from '@angular/core';
import {BryService} from '../../bry.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.css']
})
export class ViewComponent implements OnInit {

  private idUser: number;
  private user: object;
  private loader: boolean = true;
  private listOrganizations: boolean = false;

  constructor(private service: BryService, private route: ActivatedRoute, private router: Router) {
    this.service.createUrl('/users');
    this.route.params.subscribe((data) => this.idUser = data.id);
  }

  ngOnInit() {
    this.service.get(this.idUser).subscribe((data) => {
        this.user = data;
        if (this.user['organizations'].length) this.listOrganizations = true;
        this.loader = false;
      }
    );
  }

  deleteUser() {
    if (confirm('Realmente excluir o usuário?')) {
      this.loader = true;
      this.service.delete(this.idUser).subscribe(() => {
          this.router.navigate(['/users']);
        }
      );
    }
  }

}
