import {Component, OnInit} from '@angular/core';
import {BryService} from '../../bry.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.css']
})
export class ViewComponent implements OnInit {

  private idOrganization: number;
  private organization: object;
  private loader: boolean = true;
  private listUsers: boolean = false;

  constructor(private service: BryService, private route: ActivatedRoute, private router: Router) {
    this.service.createUrl('/organizations');
    this.route.params.subscribe((data) => this.idOrganization = data.id);
  }

  ngOnInit() {
    this.service.get(this.idOrganization).subscribe((data) => {
      this.organization = data;
      if (this.organization['users'].length) this.listUsers = true;
      this.loader = false;

    });
  }

  deleteOrganization() {
    if (confirm('Realmente excluir a empresa?')) {
      this.service.delete(this.idOrganization).subscribe(() => {
          this.router.navigate(['/organizations']);
        }
      );
    }
  }

}
