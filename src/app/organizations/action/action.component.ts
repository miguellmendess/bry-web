import {Component, OnInit} from '@angular/core';
import {BryService} from '../../bry.service';
import {ActivatedRoute, Router} from '@angular/router';
import {NotifyService} from '../../services/notify.service';

@Component({
  selector: 'app-action',
  templateUrl: './action.component.html',
  styleUrls: ['./action.component.css']
})
export class ActionComponent implements OnInit {

  private organizationForm = {};
  private idOrganization: number;
  private userForm = [null];
  private users: any;
  private userofOrganizations = [0];
  private loader: boolean = true;


  constructor(private service: BryService,
              private router: Router,
              private usersService: BryService,
              private route: ActivatedRoute,
              private notifyService: NotifyService) {


    this.usersService.createUrl('/users');
    this.usersService.groupList().subscribe((data) => {
      this.users = data;
    });

    this.route.params.subscribe((data) => this.idOrganization = data.id);


    this.service.createUrl('/organizations');
    if (this.idOrganization) {
      this.service.get(this.idOrganization).subscribe((data) => {
          this.organizationForm = data;
          if (this.organizationForm['users'].length > 0) {
            this.userForm = [];
            for (let u of this.organizationForm['users']) {
              this.userForm.push(u['id']);
            }
          }
          this.loader = false;
        }
      );
    } else {
      this.loader = false;
    }


  }

  ngOnInit() {
  }

  subSelectUser(index) {
    this.userofOrganizations.splice(index, 1);
    this.userForm.splice(index, 1);
  }

  addSelectUser() {
    this.userForm.push(null);
  }

  saveForm() {
    //validate users
    this.organizationForm['user_id'] = this.validateUsers();let requestService;
    let msg = "";

    if (this.idOrganization) {

      requestService = this.service.update(this.idOrganization, this.organizationForm);
      msg = "Empresa alterada com sucesso";

    } else {

      requestService = this.service.create(this.organizationForm);
      msg = "Empresa cadastrada com sucesso";

    }

    requestService.subscribe(() => {
      this.router.navigate(['/organizations']);
      this.notifyService.success(msg);
    }, (error) =>{
      this.notifyService.listErrors(error);
    });

  }


  validateUsers() {
    let idO = [];
    for (let u of this.userForm) {
      if (Number.isInteger(u)) idO.push(u);
    }
    return idO;
  }


  deleteOrganization() {
    if (confirm('Realmente excluir a empresa?')) {
      this.service.delete(this.idOrganization).subscribe(() => {
          this.router.navigate(['/organizations']);
        }
      );
    }
  }
}
