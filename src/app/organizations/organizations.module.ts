import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OrganizationsComponent } from './organizations.component';
import {RouterModule, Routes} from '@angular/router';
import { ActionComponent } from './action/action.component';
import { ViewComponent } from './view/view.component';
import {FormsModule} from '@angular/forms';
import {NgSelectModule} from '@ng-select/ng-select';


const organizationRoutes: Routes = [
  { path: 'organizations', component: OrganizationsComponent},
  { path: 'organizations/create', component: ActionComponent},
  { path: 'organizations/:id', component: ViewComponent},
  { path: 'organizations/:id/edit', component: ActionComponent}
];

@NgModule({
  declarations: [OrganizationsComponent, ActionComponent, ViewComponent],
  imports: [
    CommonModule,
    RouterModule.forRoot(organizationRoutes),
    FormsModule,
    NgSelectModule
  ]
})
export class OrganizationsModule { }
