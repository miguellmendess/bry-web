import {Component, OnInit} from '@angular/core';
import {BryService} from '../bry.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-organizations',
  templateUrl: './organizations.component.html',
  styleUrls: ['./organizations.component.css']
})
export class OrganizationsComponent implements OnInit {

  private organizations = {};
  private loader: boolean = true;

  constructor(private service: BryService, private router: Router) {

    this.service.createUrl('/organizations');

  }

  ngOnInit() {
    this.service.groupList().subscribe((data) => {
      this.organizations = data;
      this.loader = false;
    });
  }

  createOrganization() {
    this.router.navigate(['/organizations/create']);
  }


  viewOrganization(id) {
    this.router.navigate(['/organizations/' + id]);
  }

  deleteOrganization(id) {
    if (confirm('Realmente excluir a empresa?')) {
      this.service.delete(id).subscribe((data) => {
        this.organizations = data;
        this.loader = false;
        }
      );
    }
  }

}
