import {Injectable} from '@angular/core';
import PNotify from 'pnotify/dist/es/Pnotify';
import {HttpErrorResponse} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class NotifyService {

  constructor() {
  }

  private get notify() {
    return PNotify;
  }

  success(title) {
    this.notify.success(title);
  }

  error(title) {
    this.notify.error(title);
  }

  listErrors(err: HttpErrorResponse) {

    if (err.error instanceof ErrorEvent) {
      //Erro Geral JS
      this.error(err.error.message);
    } else {
      //Erro Api

      //Erro de validação de campos
      if (err.status === 422) {
        let error = err.error.errors;
        for (let er in error) {
          let msg = error[er];
          for (let e in msg) {
            this.error(msg[e]);
          }

        }
      }
    }
  }


}
