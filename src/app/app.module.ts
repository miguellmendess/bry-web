import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgSelectModule } from '@ng-select/ng-select';
import { AppComponent } from './app.component';
import {RouterModule, Routes} from '@angular/router';
import {UsersModule} from './users/users.module';
import {OrganizationsModule} from './organizations/organizations.module';
import {BryService} from './bry.service';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';


const bryRoutes: Routes = [
  { path: '', redirectTo: '/users', pathMatch: 'full' }
];

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(bryRoutes),
    UsersModule,
    OrganizationsModule,
    FormsModule,
    NgSelectModule,
    ReactiveFormsModule
  ],
  providers: [BryService],
  bootstrap: [AppComponent]
})
export class AppModule { }
