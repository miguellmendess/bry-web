import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class BryService {

  private url: string = '' ;

  //Url da APi
  private apiUrl: string = 'http://localhost:8000/api/v1';

  //String Bearer
  private bearer: string = 'Bearer ';

  //Token de autenticação
  private token: string = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImNiOGYyMzUzMmEyNDZlMmNhODU3YTE2ZGI1MDkzYWM5NTMwZjljYjJhNzE0ZTJmYzkwMzU3ODNkNmQ1NmY0YTRjNmQxODIxZjc1YWY5NTJjIn0.eyJhdWQiOiIxIiwianRpIjoiY2I4ZjIzNTMyYTI0NmUyY2E4NTdhMTZkYjUwOTNhYzk1MzBmOWNiMmE3MTRlMmZjOTAzNTc4M2Q2ZDU2ZjRhNGM2ZDE4MjFmNzVhZjk1MmMiLCJpYXQiOjE1NjUyMjQxODUsIm5iZiI6MTU2NTIyNDE4NSwiZXhwIjoxNTk2ODQ2NTg1LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.DKPW5tInMh9v2DYudfWXvK2NOrQpAf2QQst6p_NvoEJiE8soUUlbg84EaVL52wQ8Hx_Qltg_oq2oId1K_VUQqpu4gqqLjKG02NLhviLAjYFivXwjDdKg73pbmgUCM55KiGcqQKsYntuQNv7YfU6wUciRWLareb_GbLAiTcI39YG6xcW1Tar0Q1OFwhZ-s8IHQOouN1KgyBROPEN89ZmpZ6VvyNQUyEEmTGlbncMmyX_EjJ5KnhOzW2EaLfCiQSyEUKXhWAvDpwbdOxu0ffnW-52l7y7lCXGtOh46JYHoYdbk6U6qvz0zs_xomk0fIRgnHo5DfmJ8-s1JHo0YPORVWO_6CLg089pXzg7z_oLcXGUcjiAC5wd1sWokII7gDCZa-rf3o4gyk3jlocZXjb5q-iWk6h4cSdUrMr0hkzdSR0vLCCwikP0f4TODjdAKWkWu6T28G_dVkX8mNXFUa3FL6gatjPEoR3FK3dB1DVlwjTJg7yHtTAma2WRtawlNJoRByL7UqNqb7MIPdYzABRr7Ax8kSgURgabuoUHdj9h-TB8gDEVNhM6cB469fvdVng8S5kMHlIt51OoEYtnM1S0yqTVd0r5ZxYu49vdtU0O8bSNst4jCGWjLJEJBJiP3XcY3WwtQEY3v8hj4_lWZNmJUm7qPUgdwhzkDbIy92N-59Z0 ';

  //Definição do Header para autenticação
  private httpHeader: object;



  constructor(private http: HttpClient) {
    this.httpHeader = {
      headers: new HttpHeaders({
        'Authorization': this.bearer + this.token
      })
    };
  }

  createUrl(component) {
    this.url = this.apiUrl + component;
  }

  create(data) {
    return this.http.post(this.url, data, this.httpHeader);
  }

  get(id) {
    return this.http.get(this.url + '/' + id, this.httpHeader);
  }

  groupList() {
    return this.http.get(this.url, this.httpHeader);
  }

  update(id, data) {
    return this.http.put(this.url + '/' + id, data, this.httpHeader);
  }

  delete(id) {
    console.log(id);
    return this.http.delete(this.url + '/' + id, this.httpHeader);
  }


}
