<h2>Instala��o</h2>

<h3>Api</h3>

<p>Ap�s baixado o projeto, executar o comando composer install para instala��o de suas camadas.</p>

<p>Na raiz da api, alterar o arquivo ".env" com as configura��es do banco.</p>

<p>Se necess�rio, h� o banco exportado para teste. Desenvolvido em Mysql. O SQL do banco se encontra em "exportedSQL" na pasta da api</p>

<p>Caso n�o importe o banco, executar o comando "php artisan migrate". </p>
<p>*Obs: Respeitar a ordem de migrate. Deixar a migra��o 2019_08_07_181011_create_organization_user_table por ultimo</p>

<p>Ap�s a instala��o de banco e dependencias, iniciar o servi�o normalmente com o comando "artisan serve".</p>

<p>Gerar a autentica��o para desenvolvimento se registrando na plataforma. Gerar Personal Access Tokens e guardar variavel para salvar na camada web.</p>


<hr>
<h3>web</h3>


<p>Rodar comando npm install para instala��o de todas as dependencias e componentes.</p>

<p>No arquivo "web/src/app/bry.service.ts", definir rota da api na variavel "apiUrl" e configura��o da autentica��o registrada na api com o token na variavel "token'.</p>

</p>Rodar o comando ng serve para inicializar a aplica��o.</p>


<p>Arquivo com requisi��es de "users" feitas pelo postman se encontram na ra�z do projeto.</p>

Obrigado.